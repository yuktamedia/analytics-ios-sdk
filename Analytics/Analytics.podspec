Pod::Spec.new do |spec|
  spec.name         = "Analytics"
  spec.version      = "2.0.10"
  spec.summary      = "YuktaMedia Analytics sdk for iOS."
  spec.description  = <<-DESC
Analytics library is small light weight library which enable app developers to collect app usage analytics and send it to YuktaMedia, where app developers can see details in beautiful dashboard and can take action against any issues.
                   DESC

  spec.homepage     = "https://yuktamedia.com"
  spec.license      = { :type => "MIT" }


  spec.author             = { "Devendra Sane" => "devendra.sane@yuktamedia.com" }
  spec.social_media_url   = "https://twitter.com/yuktamedia"

  spec.ios.deployment_target = "14.4"
  	

  spec.source       = { :git => "https://www.bitbucket.org/yuktamedia/analytics-ios-sdk.git", :tag => "#{spec.version}" }


  spec.source_files = 
    'Analytics/Classes/*', 'Analytics/Internal/*'    

end
