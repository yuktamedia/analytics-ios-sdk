//
//  YM_Screenshot.h
//  Analytics
//
//  Created by Devendra YM on 02/05/21.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger, ScreenshotSide) {
    kScreenshotSideTop,
    kScreenshotSideBottom,
    kScreenshotSideLeft,
    kScreenshotSideRight
};

@interface YM_Screenshot : NSObject


+(UIImage *)screenshotFromAllWindows;

/* Take screenshot of different types */
+(UIImage *)screenshotFromView:(UIView *)view;
+(UIImage *)screenshotFromView:(UIView *)view side:(ScreenshotSide)screenshotSide;
+(UIImage *)screenshotFromView:(UIView *)view renderInContext:(BOOL)renderInContext;
+(UIImage *)screenshotFromView:(UIView *)view afterScreenUpdates:(BOOL)afterScreenUpdates;
+(UIImage *)screenshotFromView:(UIView *)view renderInContext:(BOOL)renderInContext afterScreenUpdates:(BOOL)afterScreenUpdates;

/* Take screenshot and get halfs of the screenshot back in a NSArray */
+(NSArray *)screenshotVerticalsFromView:(UIView *)view;
+(NSArray *)screenshotHorizontalsFromView:(UIView *)view;

/* Simply divide the given image into halfs */
+(NSArray *)verticalHalfsFromImage:(UIImage *)image;
+(NSArray *)horizontalHalfsFromImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
