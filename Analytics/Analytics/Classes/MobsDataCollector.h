////
////  MobsDataCollector.h
////  Analytics
////
////  Created by Devendra YM on 17/12/20.
////
//
//#import <Foundation/Foundation.h>
//@import GoogleMobileAds;
//NS_ASSUME_NONNULL_BEGIN
//
//@interface  MobsDataCollector : GADNativeAd {
//    GADNativeAd *nativead;
//
//}
//
//- (void)nativeAdDidRecordClick:(GADNativeAd *)nativeAd;
//- (void)nativeAdDidDismissScreen:(GADNativeAd *)nativeAd;
//- (void)nativeAdWillDismissScreen:(GADNativeAd *)nativeAd;
//- (void)nativeAdWillLeaveApplication:(GADNativeAd *)nativeAd;
//- (void)nativeAdDidRecordImpression:(GADNativeAd *)nativeAd;
//- (void)nativeAdWillPresentScreen:(GADNativeAd *)nativeAd;
//
//@end
//
//
//NS_ASSUME_NONNULL_END
